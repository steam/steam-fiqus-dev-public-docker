# ------
# Stages
# ------
stages:
  - build_docker_image
  - deploy

# ---------------------
# Docker image creation
# ---------------------
# following https://gitlab.cern.ch/strigazi/build_docker_image/-/blob/master/.gitlab-ci.yml
build_image_no_tag:
    allow_failure: true
    #rules:
    #  - if: $CI_PIPELINE_SOURCE == "schedule"
    stage:
      build_docker_image
    image:
     name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
     entrypoint: [""]
    script:
       # Following https://gitlab.com/gitlab-org/gitlab/-/issues/352762, kaniko expects an env variable container=docker
       - export container=docker
       # Prepare Kaniko configuration file
       - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
       # Build and push the image from the Dockerfile at the root of the project.
       # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
       # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
       - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE  --build-arg CERNGETDP_VERSION=latest
    except:
      - tags

build_image_no_tag_mpi:
    allow_failure: true
    #rules:
    #  - if: $CI_PIPELINE_SOURCE == "schedule"
    stage:
      build_docker_image
    image:
     name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
     entrypoint: [""]
    script:
       # Following https://gitlab.com/gitlab-org/gitlab/-/issues/352762, kaniko expects an env variable container=docker
       - export container=docker
       # Prepare Kaniko configuration file
       - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
       # Build and push the image from the Dockerfile at the root of the project.
       # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
       # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
       - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile_MPI --destination $CI_REGISTRY_IMAGE:MPI --build-arg CERNGETDP_VERSION=
    except:
      - tags

build_image_release:
  allow_failure: true
  stage:
    build_docker_image
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
      # Following https://gitlab.com/gitlab-org/gitlab/-/issues/352762, kaniko expects an env variable container=docker
      - export container=docker
      # Prepare Kaniko configuration file
      - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      # Build and push the image from the Dockerfile at the root of the project.
      # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
      # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
      - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG  --build-arg CERNGETDP_VERSION=$CI_COMMIT_TAG
  only:
    - tags
  except:
    - branches

build_image_release_mpi:
  allow_failure: true
  stage:
    build_docker_image
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
      # Following https://gitlab.com/gitlab-org/gitlab/-/issues/352762, kaniko expects an env variable container=docker
      - export container=docker
      # Prepare Kaniko configuration file
      - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
      # Build and push the image from the Dockerfile at the root of the project.
      # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
      # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
      - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile_MPI --destination $CI_REGISTRY_IMAGE:MPI_$CI_COMMIT_TAG  --build-arg CERNGETDP_VERSION=_${CI_COMMIT_TAG}
  only:
    - tags
  except:
    - branches

# ----------------------------------------
# Create Gitlab release
# ----------------------------------------
release_job:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG # only if repo tagged
  script:
    - echo "running release_job for $CI_COMMIT_TAG"
  release:
    name: '$CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG_MESSAGE'
    assets:
      links:
        - name: 'Public FiQuS Container Registry'
          url: 'https://gitlab.cern.ch/steam/steam-fiqus-dev-public-docker/container_registry'
        - name: 'Corresponding CERNGetDP Release Version ${CI_COMMIT_TAG}'
          url: 'https://gitlab.cern.ch/steam/cerngetdp/-/releases/${CI_COMMIT_TAG}'