# STEAM FiQuS Dev Public Docker

This repository is used to create a public Docker container with the requirements of the private FiQuS development repository. This Docker container can then be used to run FiQuS-dev on HTCondor.
